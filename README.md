
用來搜尋在專案底下所有Nib檔中所使用到的圖檔

用法 ruby nib_search.rb arg1 arg2

arg1: path/to/project
arg2: image file extension, e.g. png
