require 'pp'
require 'find'
require 'fileutils'

def search_all_nibs_at_path(path)
	nib_file_paths = []
	Find.find(path) do |path|
		nib_file_paths << path if path =~ /.*\.xib$/
	end
	nib_file_paths
end

def search_for_type_with_nib_path(path, type)
	lines = []
	File.open(path).each { |line|
		if line =~ /.*\.#{type}/
			line.gsub! /\t/, ''

			lines << line
		end
	}
	lines
end

def write_search_results_to_file(filepath, results)
	file = create_file(filepath)
	file.puts(results)
	file.close
end

def create_file(filepath)
	dir = File.dirname(filepath)
	FileUtils.mkdir_p(dir) unless File.directory?(dir)
	File.new(filepath, 'w+')
end

def display_commands(path, imageType)
	puts "1. Find All Nib Files at #{path}"	
	puts "2. Find All #{imageType} in all nibs"
	puts "3. Quit"
	print "Option:"
end

def find_all_image_used_in_nib_at_path(search_path, search_type)
	nib_file_paths = search_all_nibs_at_path(search_path)
	nib_file_paths.each { |path|
		pp "-------------- Current File #{File.basename(path)} --------------"
		lines = search_for_type_with_nib_path(path, search_type)

		if lines.count != 0
			puts lines
			write_search_results_to_file("./Nib_Search_Results/#{File.basename(path).gsub /.xib/, ''}.txt", lines)
		else
			pp "No #{search_type} files found"
		end
	}
end

def find_all_nibs_at_path(search_path)
	nibs = search_all_nibs_at_path(search_path)
	
	if nibs.count != 0
		# pp nibs
		write_search_results_to_file("./Nib_Search_Results/allNibs/nib_list.txt", nibs)	
	else
		pp "No Nibs found"
	end
	
end

puts "Search Path: #{ARGV[0]}"
puts "Search Type: #{ARGV[1]}"

search_path = ARGV[0]
search_type = ARGV[1]

display_commands(search_path, search_type)

while input = STDIN.gets.chomp
	case input.to_i
	when 1
		puts 1
		find_all_nibs_at_path(search_path)
	when 2
		puts 2
		find_all_image_used_in_nib_at_path(search_path, search_type)
	when 3
		puts 3
		exit(0)
	else
		puts "Wrong Option!"
	end
	display_commands(search_path, search_type)
end